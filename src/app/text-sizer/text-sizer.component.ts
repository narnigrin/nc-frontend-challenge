import { Component, AfterViewInit, viewChild, ElementRef, ChangeDetectorRef, Input, SimpleChanges } from "@angular/core";

@Component({
    selector: 'text-sizer',
    standalone: true,
    templateUrl: './text-sizer.component.html',
    styleUrl: './text-sizer.component.scss',
})
export class TextSizerComponent implements AfterViewInit {
    @Input() text = '';
    @Input() factor = 0.9; // A magic number, unfortunately
    fontSize = 9;
    cssWhiteSpace = 'nowrap';
    element = viewChild<ElementRef>("element");
    oldTextContent = '';

    constructor(private cdRef:ChangeDetectorRef){}

    calculateSize(): void {
        const maxSize = 20, minSize = 1;
        this.cssWhiteSpace = 'nowrap';

        // returns a positive number if element is wider than viewport; negative if narrower
        const diff = (): number => {
            let elementWidth = this.element()?.nativeElement.offsetWidth,
                clientWidth = this.element()?.nativeElement.parentElement.clientWidth * this.factor;
            return elementWidth - clientWidth;
        }

        let isOverflown = false;
        const operator = (diff() > 0) ? -1 : 1, // Opposite sign to the initial value of diff()
              opMultiple = 0.01;

        let i = this.fontSize;

        while (!isOverflown && i <= maxSize && i >= minSize) {
            i += operator * opMultiple;
            this.fontSize = i;
            isOverflown = diff() * operator > 0; // becomes true when the element crosses the viewport boundary
            this.cdRef.detectChanges();
        }
       
        if (operator > 0) {
            i -= operator * opMultiple;
        }
        i = Math.min(Math.max(i, minSize), maxSize);

        this.fontSize = i;
        this.cssWhiteSpace = 'normal';
        this.cdRef.detectChanges();
    }

    ngAfterViewInit(): void {
        this.calculateSize();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes["text"].currentValue != changes["text"].previousValue) {
            this.calculateSize();
        }
    }
}