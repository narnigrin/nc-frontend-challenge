import { Component, OnInit, OnDestroy } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { TextSizerComponent } from './text-sizer/text-sizer.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FormsModule, TextSizerComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'FrontendChallenge';
  today = new Date().toISOString().split("T")[0];
  countdownLabel = 'Your event';
  countdownDate = this.today;
  countdownOutput = '';
  intervalId: any;

  update = (): void => {
    localStorage.setItem("label", this.countdownLabel);
    localStorage.setItem("date", this.countdownDate);
  }

  reset = (): void => {
    this.countdownDate = '';
    this.countdownOutput = '';
    this.update();
  }

  updateCountdownOutput = (): void => {
    const enteredDate = new Date(this.countdownDate);

    // Get out of here if enteredDate isn't a date
    if (!(enteredDate instanceof Date && !isNaN(enteredDate.getTime()))) {
      this.reset();
      return;
    }

    const curUTC = Date.now();
    // Set hours to 0 below to prevent UTC vs local timezones causing confusion in the calculations
    const enteredUTC = enteredDate.setHours(0).valueOf();

    // Leave if we're given a date in the past
    if (enteredUTC <= curUTC) {
      this.reset();
      return;
    }
    let diffSeconds = Math.floor((enteredUTC - curUTC) / 1000);

    let divisors = [24, 60, 60];
    let results = [];
    for (let i = 0; i < divisors.length; i++) {
      let divisor = divisors.reduce((acc, cur): number => acc * cur, 1);
      results[i] = Math.floor(diffSeconds / divisor);
      diffSeconds -= results[i] * divisor;
      divisors[i] = 1;
    }

   let days = (results[0] == 1) ? "day" : "days";
   this.countdownOutput = `${results[0]} ${days}, ${results[1]} h, ${results[2]} m, ${diffSeconds} s`;
  }

  ngOnInit() {
    this.countdownLabel = localStorage.getItem("label") || "";
    this.countdownDate = localStorage.getItem("date") || "";

    this.updateCountdownOutput();
    this.intervalId = setInterval(this.updateCountdownOutput, 1000);
  }
  ngOnDestroy() {
    clearInterval(this.intervalId);
  }
}
