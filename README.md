# This project

This is my answer to the frontend challenge at https://github.com/NaturalCycles/FrontendChallenge.

The app is deployed at https://nc-frontend-challenge.vercel.app/.

## Setting up locally

1. Clone the repo and run `npm install`.
2. Run `npm run start` to start a dev server. The app will be available at `http://localhost:4200`.

## Comments

* While the instructions said for the text to "always [cover] the entire screen width", I have made some judgement calls on max and min font sizes, in order to avoid ridiculous/illegible edge cases. If the title is too long to fit on one line at the minimum font size, it is allowed to wrap. In a real-world scenario, this would ideally have been discussed with the designer and/or client to get to the best (or least bad) solution.
* The design file could be interpreted as saying the form fields ought to stretch and shrink with the viewport. Another possible interpretation is for them to have fixed sizes. I chose the latter for simplicity, keeping in mind that few if any users would try to use a mobile app on a screen narrower than 300-400px.
* There is a (minor) conflict between the design file and the README on the exact format of the countdown output. I chose to follow the README.

## Potential improvements

* There are occasional (rare) bugs with the text fit script. These **could** be due to how Angular handles/queues changes to views and my limited understanding thereof, and can probably be solved by improving understanding of those topics, refactoring code to use more 'vanilla' JS for those parts of the logic, or even by switching the current solution out for a third-party plugin, of which there exist plenty. *(For a production build, I would probably have opted for a third-party plugin as my first option. However, here, the text sizing was at least half of the exercise, so it would have felt wrong not to make it from scratch.)*
* The form could do with more validation and feedback:
    - Currently, it checks that the date is indeed a date and that it is in the future. It gives no feedback to the user if either of these rules are broken. A custom date picker might also be a good improvement on trusting the browser's `input type="date"` implicitly.
    - There is no validation on the label/event title field. In particular, this means it can be arbitrarily long. (If it is too long, it *will* wrap; see **Comments** for more on this.) One might want to limit the length to completely avoid wrapping, or set some other restrictions.
* Instead of allowing extremely long labels to line break, one might have clipped the text with an ellipsis (or some other solution or combination). This is a decision that the designer/customer is better placed to make than the developer.
* There is most likely space for performance improvements.
* There might be best practices re. code structure, code style or similar, either in the Angular community or in your team, that I am not following due to lack of awareness.
* If the application needs to be supported in older user agents, the CSS must be refactored. (In particular, custom properties, calc() and clamp() need to be removed.)
* Angular user forums seem to largely be of the opinion that template forms should be abandoned in favour of reactive forms (in ALL use cases), so making that change for the sake of keeping the code base extensible and up-to-date might be a good idea.